﻿using System.Text;
using NLog;
using NLog.Config;
using NLog.Targets;
using NLog.Targets.Wrappers;

namespace VionisDevKernel.Utils
{
    public static class NLogInstaller
    {
        private static bool isBuildedForTests;
        public static void BuildForTests()
        {
            if (isBuildedForTests)
                return;
            
            var commonLayout = @"[${level}] - [${date:format=HH\:mm\:ss.fff} ${logger}] - ${message}";
            var config = new LoggingConfiguration();

            //add console target
            var consoleTarget = new ConsoleTarget("logconsole");
            config.AddTarget("consoleTarget", new AsyncTargetWrapper(consoleTarget));
            consoleTarget.Encoding = Encoding.UTF8;
            consoleTarget.Layout = commonLayout;

            config.AddRule(LogLevel.Debug, LogLevel.Fatal, consoleTarget);
            LogManager.Configuration = config;

            isBuildedForTests = true;
        }

        public static void Build()
        {
            var commonLayout = @"${date:format=HH\:mm\:ss.fff} ${threadname} ${logger} ${message}";
            var commonFileName = @"${basedir}/logs/full.log";
            
            var errorLayout = @"${date:format=HH\:mm\:ss.fff} ${threadname} ${logger} ${message} Process: ${processinfo}; GC: ${gc:property=TotalMemory} bytes;";
            var errorFileName = @"${basedir}/logs/errors.log";

            var fatalLayout = @"${date:format=HH\:mm\:ss.fff} ${threadname} ${logger} ${message} Process: ${processinfo} GC: ${gc:property=TotalMemory} bytes; PagedSystemMemorySize: ${processinfo:property=PagedSystemMemorySize64} bytes";
            var fatalFileName = @"${basedir}/logs/fatals.log";
            
            var config = new LoggingConfiguration();

            //add console target
            var consoleTarget = new ConsoleTarget("logconsole");
            config.AddTarget("consoleTarget", new AsyncTargetWrapper(consoleTarget));
            consoleTarget.Encoding = Encoding.UTF8;
            consoleTarget.Layout = commonLayout;
            
            //add file target
            var fileTarget = new FileTarget();
            var asyncFile = new AsyncTargetWrapper(fileTarget);
            config.AddTarget("file", asyncFile);
            fileTarget.Encoding = Encoding.UTF8;
            fileTarget.FileName = commonFileName;
            fileTarget.Layout = commonLayout;
            fileTarget.KeepFileOpen = true;

            //add error target
            var errorsFileTarget = new FileTarget();
            var asyncError = new AsyncTargetWrapper(errorsFileTarget);
            config.AddTarget("errorsFile", asyncError);
            errorsFileTarget.Encoding = Encoding.UTF8;
            errorsFileTarget.FileName = errorFileName;
            errorsFileTarget.Layout = errorLayout;
            errorsFileTarget.KeepFileOpen = true;

            //add fatal target
            var fatalsFileTarget = new FileTarget();
            var asyncFatal = new AsyncTargetWrapper(fatalsFileTarget);
            config.AddTarget("fatalFile", asyncFatal);
            fatalsFileTarget.Encoding = Encoding.UTF8;
            fatalsFileTarget.FileName = fatalFileName;
            fatalsFileTarget.Layout = fatalLayout;
            fatalsFileTarget.KeepFileOpen = true;

            //setup archive settings
            SetupArchiveSettingsToTarget(fileTarget, @"full");
            SetupArchiveSettingsToTarget(errorsFileTarget, @"errors");
            SetupArchiveSettingsToTarget(fatalsFileTarget, @"fatals");

            config.AddRule(LogLevel.Debug, LogLevel.Fatal, fileTarget);
            config.AddRule(LogLevel.Error, LogLevel.Fatal, consoleTarget);
            config.AddRule(LogLevel.Error, LogLevel.Fatal, errorsFileTarget);
            config.AddRule(LogLevel.Fatal, LogLevel.Fatal, fatalsFileTarget);
            
            LogManager.Configuration = config;
        }

        private static void SetupArchiveSettingsToTarget(FileTarget target, string name)
        {
            target.ArchiveEvery = FileArchivePeriod.Day;
            target.ArchiveOldFileOnStartup = true;
            target.ArchiveFileName = @"${basedir}/logs/archives/" + name + @"_{#}.log";
            target.ArchiveNumbering = ArchiveNumberingMode.Date;
            target.ArchiveDateFormat = @"yyyy-MM-dd_HH.mm.ss";
            target.MaxArchiveFiles = 3;
        }
    }
}