﻿using System;
using System.Reflection;
using Config.Net;
using NLog;

namespace VionisDevKernel
{
    public class ConfigReaderInstaller
    {        
        public static void Setup<T>(T settings, string configName, bool initializateAllFields) where T : class 
        {
            if (settings == null)
                throw new ArgumentNullException(nameof(settings));

            settings = new ConfigurationBuilder<T>()
                .UseJsonFile(configName)
                .Build();

            if (initializateAllFields)
                foreach (var p in settings.GetType().GetRuntimeProperties())
                    p.SetValue(settings, Convert.ChangeType(p.GetValue(settings), p.PropertyType), null);
        }
    }
}