﻿using System.Collections.Generic;

namespace VionisDevKernel.Utils
{
    public class Pool<T> where T : class, new()
    {
        private readonly List<T> items;
        private int current;
        public Pool(int capacity)
        {
            items = new List<T>(capacity);
        }

        public T Take()
        {
            if (current >= items.Count)
                return null;

            return items[current++];
        }

        public void Free()
        {
            //items[current]
            //TODO: don`t completed this class
        }
    }
}