﻿namespace VionisDevKernel.Utils
{
    public struct Details
    {
        public readonly bool IsOk;
        public readonly string Info;

        private Details(bool isOk, string info)
        {
            IsOk = isOk;
            Info = info;
        }

        public override string ToString()
        {
            return "Info: " + Info;
        }        

        public static Details Ok(string details="") => new Details(true, details);

        public static Details Fail(string details="") => new Details(false, details);
    }
}