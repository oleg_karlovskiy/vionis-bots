﻿using System;
using System.Collections.Generic;
using VionisDevKernel.Collections;
using Xunit;

namespace VionisDevKernel.VionisDevKernel.Tests
{
    public class ListExtensionsTest
    {
        [Fact]
        public void TestRemoveList()
        {
            List<ListExtensionsTest> list = null;
            Assert.Throws<NullReferenceException>(() => list.FastRemoveAt(0));
            list = new List<ListExtensionsTest>();
            for (int i = 0; i < 10; i++)
            {
                list.Add(new ListExtensionsTest());
            }

            for (int i = 0; i < list.Count; i++)
            {
                list.FastRemoveAt(ref i);
            }

            Assert.Empty(list);
        }
    }
}