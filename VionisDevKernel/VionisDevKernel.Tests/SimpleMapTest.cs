﻿using System;
using System.Collections;
using System.Collections.Generic;
using VionisDevKernel.Collections;
using Xunit;
using Xunit.Abstractions;
using System.Collections.Specialized;

namespace VionisDevKernel.VionisDevKernel.Tests
{    
    public class SimpleMapTest : BaseTest
    {
        public SimpleMapTest(ITestOutputHelper outq) : base(outq)
        {
        }

        //[Theory]
        //[InlineData(-1)]
        //[InlineData(0)]
        //[InlineData(1)]
        [Fact]
        public void InitTest()
        {            
            Assert.Throws<IndexOutOfRangeException>(() => new SimpleMap<string, char>(-5));
            Assert.Throws<IndexOutOfRangeException>(() => new SimpleMap<string, char>(0));
            new SimpleMap<string, char>(5);
        }

        [Fact]
        public void AddTest()
        {
            var map = new SimpleMap<char, long>(3);
            
            var isOk = true;
            isOk = isOk && map.Add('q', long.MaxValue);
            isOk = isOk && map.Add('w', long.MinValue);
            isOk = isOk && map.Add('d', 123);
            Assert.True(isOk);

            output.WriteLine("Map Content: " + map);

            Assert.Equal('q', map.Get1(0));
            Assert.Equal('w', map.Get1(1));
            Assert.Equal('d', map.Get1(2));

            Assert.Equal(long.MaxValue, map.Get2(0));
            Assert.Equal(long.MinValue, map.Get2(1));
            Assert.Equal(123, map.Get2(2));
        }

        [Fact]
        public void AddRemoveTest()
        {
            var map = new SimpleMap<int, string>(100);

            for (int i = 0; i < map.Capacity; i++)
            {
                map.Add(i, i.ToString());
                output.WriteLine("Add: " + i);
            }

            output.WriteLine("Map Before Adding: " + map);
            int repeatCount = 10;
            for (int i = 0; i < repeatCount; i++)
            {
                var removeIdx = new Random().Next(0, map.Size);
                output.WriteLine("Removing... {0} idx. Map Size After Delete: {1}", removeIdx, map.Size - 1);
                map.RemoveAt(removeIdx);
            }

            output.WriteLine("Map Before Remove: " + map);

            for (int i = 0; i < map.Size; i++)
            {
                Assert.Equal(map.Get1(i).ToString(), map.Get2(i));
            }

            Assert.Throws<IndexOutOfRangeException>(() => map.RemoveAt(int.MaxValue));            
            Assert.Throws<IndexOutOfRangeException>(() => map.RemoveAt(map.Size));            

            for (int i = 0; i < map.Size; i++)
            {
                Assert.Equal(map.Get1(i), int.Parse(map.Get2(i)));
            }
        }

        private class TwoStrings
        {
            public string s1;
            public string s2;
        }

        //[Fact]
        //public void BenchmarkComparison()
        //{
        //    var repeatCount = 5;
        //    var cap = 1000000;
        //    var map = new SimpleMap<string, string>(cap);
        //    var dict = new Dictionary<string, string>(cap);
        //    var hashtable = new Hashtable(cap);
        //    var list = new List<TwoStrings>();
        //    CollectionsUtil.CreateCaseInsensitiveSortedList();
        //    var testString = "test";
        //    var testTwoStrings = new TwoStrings();
        //    testTwoStrings.s1 = "test";
        //    testTwoStrings.s2 = "test2";

        //    output.WriteLine("Add Test");
        //    output.WriteLine("");
        //    //adding test
        //    for (int q = 0; q < repeatCount; q++)
        //    {
        //        var dictBefore = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        for (int i = 0; i < cap; i++)
        //        {
        //            dict.Add(i.ToString(), testString);
        //        }
        //        var dictAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        //        var simpleMapBefore = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        for (int i = 0; i < cap; i++)
        //        {
        //            map.Add(i.ToString(), testString);
        //        }
        //        var simpleMapAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        //        var hashBefore = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        for (int i = 0; i < cap; i++)
        //        {
        //            hashtable.Add(i.ToString(), testString);
        //        }
        //        var hashAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        //        var llistBefore = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        for (int i = 0; i < cap; i++)
        //        {
        //            testTwoStrings.s1 = i.ToString();
        //            testTwoStrings.s2 = testString;
        //            list.Add(testTwoStrings);
        //        }
        //        var llistAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        //        output.WriteLine("Dict: {0}ms, SimpleMap: {1}ms, Hashtable: {2}ms, List: {3}ms", dictAfter - dictBefore, simpleMapAfter - simpleMapBefore, hashAfter - hashBefore, llistAfter - llistBefore);
        //        output.WriteLine("{0}% simplemap fastest dict ", (float)(dictAfter - dictBefore) / (simpleMapAfter - simpleMapBefore) * 100- 100);
        //        output.WriteLine("{0}% simplemap fastest list", (float)(llistAfter - llistBefore) / (simpleMapAfter - simpleMapBefore) * 100- 100);
        //        output.WriteLine("{0}% simplemap fastest hashtable \n", (float)(hashAfter - hashBefore) / (simpleMapAfter - simpleMapBefore) * 100- 100);
        //        var lastIter = repeatCount - 1;
        //        if (q != lastIter)
        //        {
        //            dict.Clear();
        //            map.Clear();
        //            hashtable.Clear();
        //        }
        //        GC.Collect();
        //    }
            
        //    output.WriteLine("Get Test");
        //    output.WriteLine("");
        //    var stringToPaste = "";
        //    for (int q = 0; q < repeatCount; q++)
        //    {
        //        var dictBefore = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        for (int i = 0; i < cap; i++)
        //        {
        //            stringToPaste = dict[i.ToString()];
        //        }
        //        var dictAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        //        var simpleMapBefore = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        for (int i = 0; i < cap; i++)
        //        {
        //            stringToPaste = map.Get2(i);
        //        }
        //        var simpleMapAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        //        var hashBefore = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        for (int i = 0; i < cap; i++)
        //        {
        //            stringToPaste = hashtable[i.ToString()].ToString();
        //        }
        //        var hashAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        //        var llistBefore = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        for (int i = 0; i < cap; i++)
        //        {
        //            stringToPaste = list[i].s2;
        //        }
        //        var llistAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        //        output.WriteLine("Dict: {0}ms, SimpleMap: {1}ms, Hashtable: {2}ms, List: {3}ms", dictAfter - dictBefore, simpleMapAfter - simpleMapBefore, hashAfter - hashBefore, llistAfter - llistBefore);
        //        output.WriteLine("{0}% simplemap fastest dict ", (float)(dictAfter - dictBefore) / (simpleMapAfter - simpleMapBefore) * 100- 100);
        //        output.WriteLine("{0}% simplemap fastest list", (float)(llistAfter - llistBefore) / (simpleMapAfter - simpleMapBefore) * 100- 100);
        //        output.WriteLine("{0}% simplemap fastest hashtable \n", (float)(hashAfter - hashBefore) / (simpleMapAfter - simpleMapBefore) * 100- 100);
        //        GC.Collect();
        //    }
            
        //    output.WriteLine("Remove Test");
        //    output.WriteLine("");
        //    for (int q = 0; q < repeatCount; q++)
        //    {
        //        var dictBefore = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        for (int i = 0; i < cap; i++)
        //        {
        //            dict.Remove(i.ToString());
        //        }
        //        var dictAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        //        var simpleMapBefore = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        while (map.Size != 0)
        //        {
        //            map.RemoveAt(0);
        //        }
        //        var simpleMapAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        //        var hashBefore = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        for (int i = 0; i < cap; i++)
        //        {
        //            hashtable.Remove(i.ToString());
        //        }
        //        var hashAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        //        var llistBefore = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        var llistAfter = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() + 10000; //o(n) in list remove... its veeeeeery long...

        //        var llist2Before = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        //        while (list.Count != 0)
        //        {
        //            list.FastRemoveAt(0);
        //        }
        //        var llist2After = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        //        output.WriteLine("Dict: {0}ms, SimpleMap: {1}ms, Hashtable: {2}ms, List: {3}ms, FastList: {4}ms", dictAfter - dictBefore, simpleMapAfter - simpleMapBefore, hashAfter - hashBefore, llistAfter - llistBefore, llist2After - llist2Before);
        //        output.WriteLine("{0}% simplemap fastest dict ", (float)(dictAfter - dictBefore) / (simpleMapAfter - simpleMapBefore) * 100 - 100);
        //        output.WriteLine("{0}% simplemap fastest list", (float)(llistAfter - llistBefore) / (simpleMapAfter - simpleMapBefore) * 100 - 100);
        //        output.WriteLine("{0}% simplemap fastest fastlist", (float)(llist2After - llist2Before) / (simpleMapAfter - simpleMapBefore) * 100 - 100);
        //        output.WriteLine("{0}% simplemap fastest hashtable \n", (float)(hashAfter - hashBefore) / (simpleMapAfter - simpleMapBefore) * 100 - 100);
                
        //        output.WriteLine("{0}% fastlist fastest list \n", (float)(llistAfter - llistBefore) / (llist2After - llist2Before) * 100 - 100);

        //        var lastIter = repeatCount - 1;
        //        if (q != lastIter)
        //        {
        //            for (int i = 0; i < cap; i++)
        //            {
        //                dict.Add(i.ToString(), testString);
        //                map.Add(i.ToString(), testString);
        //                hashtable.Add(i.ToString(), testString);
        //                testTwoStrings.s1 = i.ToString();
        //                testTwoStrings.s2 = testString;
        //                list.Add(testTwoStrings);
        //            }
        //        }
        //        GC.Collect();
        //    }
        //}
    }

}

