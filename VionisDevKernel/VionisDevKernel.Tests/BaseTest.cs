﻿using System;
using System.IO;
using VionisDevKernel.Utils;
using Xunit.Abstractions;

namespace VionisDevKernel.VionisDevKernel.Tests
{
    public abstract class BaseTest : IDisposable
    {
        protected ITestOutputHelper output;
        private StringWriter stringWriter = new StringWriter();

        protected BaseTest(ITestOutputHelper outq)
        {            
            NLogInstaller.BuildForTests();
            output = outq;
            Console.SetOut(stringWriter);
        }

        protected string GetConsoleOutput()
        {
            return stringWriter.ToString();
        }

        public void Dispose()
        {            
            output.WriteLine("\n\nConsole Printed: \n" + stringWriter + "\n");

            stringWriter.Dispose();
        }
    }
}