﻿using Xunit;

namespace VionisDevKernel.VionisDevKernel.Tests
{
    public static class XunitExtensions
    {
        public static void EqualAsrt<T1, T2>(T1 expected, T2 actual, string expectedName)
        {
            if (!expected.Equals(actual))
                Assert.True(expected.Equals(actual), "Expected (" + expectedName + "): " + expected + "\n Actual: " + actual);
            
        }
    }
}