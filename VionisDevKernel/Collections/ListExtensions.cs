﻿using System.Collections.Generic;

namespace VionisDevKernel.Collections
{
    public static class ListExtensions
    {
        public static void FastRemoveAt<T>(this List<T> list, ref int i)
        {
            list.FastRemoveAt(i);
            i--;
        }

        public static void FastRemoveAt<T>(this List<T> list, int i)
        {
            list[i] = list[list.Count - 1];
            list.RemoveAt(list.Count - 1);
        }

        public static T GetOrDefault<T>(this T[] array, int i)
        {
            return i >= array.Length || i < 0 ? default(T) : array[i];
        }
    }
}