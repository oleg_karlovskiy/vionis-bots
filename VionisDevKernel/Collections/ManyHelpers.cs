﻿using System;

namespace VionisDevKernel.Collections
{
    public static class ManyHelpers
    {
        public static bool SafeBoolParse(this string strbool)
        {
            bool.TryParse(strbool, out var b);
            return b;
        }

        public static int SafeIntParse(this string strint)
        {
            int.TryParse(strint, out var value);
            return value;
        }
    }
}