﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace VionisDevKernel.Collections
{
    public class SimpleMap<T1, T2>
    {
        private readonly T1[] firsts;
        private readonly T2[] seconds;

        public int Size { get; private set; }
        public int Capacity { get; }

        public SimpleMap(int capacity)
        {
            if (capacity <= 0)
                throw new IndexOutOfRangeException("Capacity incorrect value: " + capacity);

            Capacity = capacity;
            firsts = new T1[capacity];
            seconds = new T2[capacity];
        }

        public T1 Get1(int i) => firsts[i];
        public T2 Get2(int i) => seconds[i];

        public bool Add(T1 item1, T2 item2)
        {
            return Add(ref item1, ref item2);
        }

        public bool Add(ref T1 item1, ref T2 item2)
        {
            if (Size >= Capacity)
                return false;

            firsts[Size] = item1;
            seconds[Size] = item2;
            Size++;

            return true;
        }

        public void RemoveAt(ref int i)
        {
            RemoveAt(i);
            i--;
        }

        public void RemoveAt(int i)
        {
            if (i < 0 || i >= Size)
                throw new IndexOutOfRangeException(string.Format("Index incorrect value: {0}, Need it beetween 0 and {1} inclusive", i, Size - 1));

            firsts[i] = firsts[Size - 1];
            seconds[i] = seconds[Size - 1];

            firsts[Size - 1] = default(T1);
            seconds[Size - 1] = default(T2);

            Size--;
        }

        public void Clear()
        {
            for (int i = 0; i < Size; i++)
            {
                firsts[i] = default(T1);
                seconds[i] = default(T2);
            }
            Size = 0;
        }

        public override string ToString()
        {
            return "{" + string.Join(",", firsts.Take(Size).Zip(seconds.Take(Size), (first, second) => first + ":" + second).ToArray()) + "}";
        }
    }
}