﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using Telegram.Bot;
using Telegram.Bot.Args;
using VionisBotsKernel.Common;
using VionisDevKernel.Utils;

namespace VionisBotsKernel.Telegram
{
    public sealed class TelegramHandler : BaseBotHandler
    {
        # region Singleton
        private static readonly Lazy<TelegramHandler> Lazy = new Lazy<TelegramHandler>(() => new TelegramHandler());

        public static TelegramHandler Instance => Lazy.Value;
        #endregion

        public static Logger log = LogManager.GetCurrentClassLogger();

        public override BotType Type { get; } = BotType.Telegram;

        private TelegramBotClient botClient;

        public void Setup(string telegramToken, BotCore botCore) 
        {
            BaseSetup(botCore);            

            botClient = new TelegramBotClient(telegramToken);
            botClient.OnMessage += RecieveMessageFromUser;
            botClient.StartReceiving();            
        }

        private void RecieveMessageFromUser(object sender, MessageEventArgs messageEventArgs)
        {
            var details = multiThreadHandler.Read(new CommonMessage
            (
                apiMessageChatId: messageEventArgs.Message.Chat.Id,
                messageText: messageEventArgs.Message.Text,
                type: Type,
                context: messageEventArgs.Message
            ));
            if (!details.IsOk)
                log.Error("RecieveMessageFromUser: " + details.Info);
            else
                log.Debug($"First phase recieive message success. From {messageEventArgs.Message.Chat.Id} chatID");
            
        }

        public override Task SendMessage(CommonMessage message)
        {
            log.Debug($"Send response. To {message.ApiMessageChatId} chatID");
            return botClient.SendTextMessageAsync(message.ApiMessageChatId, message.MessageText);
        }

        public override Details ShutDown()
        {
            botClient.StopReceiving();
            while (botClient.IsReceiving)
            {
                Thread.Sleep(100);
            }
            return Details.Ok();
        }
    }
}