﻿using Config.Net;

namespace VionisBotsKernel.Config
{
    //https://github.com/aloneguid/config
    public interface IBaseSettings
    {
        //telegram
        [Option(DefaultValue = "PasteBotToken to this - you need to talk to Bot Father https://t.me/botfather on Telegram. Register a bot with him and get an access token ")]
        string TelegramBotToken { set; get; }        
    }
}