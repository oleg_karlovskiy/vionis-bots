﻿namespace VionisBotsKernel.Common
{
    public class CommonMessage
    {
        public long Guid { get; }
        public long ApiMessageChatId { get; }
        public string MessageText { get; }
        public BotType Type { get; }
        public object Context { get; }

        public CommonMessage(long apiMessageChatId, string messageText, BotType type, object context)
        {
            Guid = GuidGen.NextGuid();
            ApiMessageChatId = apiMessageChatId;
            MessageText = messageText;
            Type = type;
            Context = context;
        }


        public CommonMessage(string response, CommonMessage replyMessage)
        {
            Guid = GuidGen.NextGuid();
            ApiMessageChatId = replyMessage.ApiMessageChatId;
            Type = replyMessage.Type;
            Context = replyMessage.Context;

            MessageText = response;
        }
    }
}