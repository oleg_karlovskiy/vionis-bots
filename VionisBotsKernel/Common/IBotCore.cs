﻿using System.Threading.Tasks;
using VionisDevKernel.Utils;

namespace VionisBotsKernel.Common
{
    public interface IBotCore
    {
        Task SendMessage(CommonMessage message);
        Details ShutdownAll();
    }
}