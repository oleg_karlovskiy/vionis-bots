﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NLog;
using VionisMessagesBus;
using VionisDevKernel.Collections;
using VionisDevKernel.Utils;

namespace VionisBotsKernel.Common
{
    public sealed class BotCore : IBotCore
    {
        public static Logger log = LogManager.GetCurrentClassLogger();

        private readonly object sync = new object();

        private BaseBotHandler[] handlers = new BaseBotHandler[Enum.GetNames(typeof(BotType)).Length];
        private BotRequestHandler<CommonMessage> botRequestHandler;
        public MultiThreadHandler<CommonMessage, CommonMessage> MultiThreadHandler { get; } = new MultiThreadHandler<CommonMessage, CommonMessage>();

        public BotCore()
        {
            botRequestHandler = new BotRequestHandler<CommonMessage>(this);
            MultiThreadHandler.Start(typeof(BotMessagesHandler), botRequestHandler);
        }

        public void SetHandler(BotType type, BaseBotHandler handler)
        {
            handlers[(int) type] = handler;
        }

        public Task SendMessage(CommonMessage message)
        {
            lock (sync)
            {
                var hndlr = handlers.GetOrDefault((int) message.Type);
                if (hndlr == null)
                {
                    log.Fatal($"Handler don`t setted to {message.Type} type bot");
                    return null;
                }

                return hndlr.SendMessage(message);
            }
                
        }

        public Details ShutdownAll()
        {
            Details details;
            var failedShutdownedHandlers = new List<Details>(handlers.Length);
            for (int i = 0; i < handlers.Length; i++)
            {
                details = handlers[i].ShutDown();
                if (!details.IsOk)
                    failedShutdownedHandlers.Add(details);
            }

            details = MultiThreadHandler.ShutDownGracefully();
            if (!details.IsOk)
                failedShutdownedHandlers.Add(details);

            return failedShutdownedHandlers.Count == 0 ? Details.Ok() : Details.Fail("ShutdownAll FailedInfo: " + string.Join(", ", failedShutdownedHandlers.Select(q => q.ToString()).ToArray()));
        }
    }
}