﻿using System.Threading.Tasks;
using VionisDevKernel.Utils;
using VionisMessagesBus;

namespace VionisBotsKernel.Common
{
    public abstract class BaseBotHandler
    {
        public abstract BotType Type { get; }

        protected MultiThreadHandler<CommonMessage, CommonMessage> multiThreadHandler;
        
        public void BaseSetup(BotCore botCore)
        {
            multiThreadHandler = botCore.MultiThreadHandler;
            botCore.SetHandler(Type, this);
        }

        public abstract Task SendMessage(CommonMessage message);

        public abstract Details ShutDown();
    }
}