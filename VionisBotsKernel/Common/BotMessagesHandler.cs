﻿using VionisMessagesBus;
using VionisDevKernel.Utils;

namespace VionisBotsKernel.Common
{
    public class BotMessagesHandler : ICoreMessageHandler<CommonMessage, BaseCommonHandler<CommonMessage>>
    {
        public Details Handle(CommonMessage message, BaseCommonHandler<CommonMessage> botRequestHandler)
        {       
            return botRequestHandler.Handle(new CommonMessage("Yes", message));
        }
    }
}