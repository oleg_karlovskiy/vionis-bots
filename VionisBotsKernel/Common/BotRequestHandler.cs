﻿using System.Threading.Tasks;
using VionisMessagesBus;

namespace VionisBotsKernel.Common
{
    public class BotRequestHandler<TCommonMessage> : BaseRequestsHandler<TCommonMessage>
        where TCommonMessage : CommonMessage
    {
        private readonly BotCore botCore;

        public BotRequestHandler(BotCore botCore)
        {
            this.botCore = botCore;
        }

        protected override Task SendMsg(TCommonMessage message)
        {
            return botCore.SendMessage(message);
        }
    }
}