﻿namespace VionisBotsKernel.Common
{
    public static class GuidGen
    {
        private static long lastGuid;

        private static readonly object sync = new object();

        public static long NextGuid()
        {
            lock (sync)            
                return lastGuid++;
        }

        public static void Clear()
        {
            lock (sync)
                lastGuid = 0;
        }
    }
}