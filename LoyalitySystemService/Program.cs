﻿using NLog;
using VionisDevKernel;
using VionisDevKernel.Utils;

namespace LoyalitySystemService
{
    sealed class Program : BaseEntryPoint
    {
        public Logger log = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            var pr = new Program();
            pr.SetupLogger();
            pr.SetupAndStart();
        }

        private void SetupLogger()
        {
            NLogInstaller.Build();
            log.Info("NLogger successfully builded");
        }


        protected override void StartMainLogic()
        {
            
        }

        protected override void Shutdown()
        {
            
        }
    }
}
