﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf;
using NLog;
using RabbitAdapter.Protobuf;
using RabbitMQ.Client;
using VionisDevKernel.VionisDevKernel.Tests;
using Xunit;
using Xunit.Abstractions;

namespace RabbitAdapter.RabbitAdapter.Tests
{
    public class SendReceiveSimpleTest : BaseTest
    {
        public static Logger log = LogManager.GetCurrentClassLogger();

        public SendReceiveSimpleTest(ITestOutputHelper outq) : base(outq)
        {
        }

        [Fact]
        public void Main()
        {
            log.Info("Start!");
            var task2 = new Task(() => new Receiver().ReceiveTest());

            task2.Start();
            Thread.Sleep(1000);
            SendTest();
            task2.Wait();
        }


        [Fact]
        public void Proto()
        {
           

            

            
        }


        public void SendTest()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "hello",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var toSerialize = new SearchRequest
                {
                    PageNumber = 3,
                    Query = "test123123123123",
                    ResultPerPage = 5
                };

                var body = toSerialize.ToByteArray();

                for (int i = 0; i < 1; i++)
                {
                    channel.BasicPublish(
                        exchange: "",
                        routingKey: "hello",
                        basicProperties: null,
                        body: body);
                    log.Info("Sent");
                }

            }
        }
    }
}