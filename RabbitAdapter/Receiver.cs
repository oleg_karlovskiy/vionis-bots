﻿using System;
using System.Text;
using System.Threading;
using NLog;
using RabbitAdapter.Protobuf;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitAdapter
{
    public class Receiver
    {

        public static Logger log = LogManager.GetCurrentClassLogger();

        public void ReceiveTest()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "hello",
                            durable: false,
                            exclusive: false,
                            autoDelete: false,
                            arguments: null);
                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        var recieved = SearchRequest.Parser.ParseFrom(body);
                        log.Info($"Received: {recieved.PageNumber}, {recieved.Query}, {recieved.ResultPerPage}");
                    };
                    channel.BasicConsume(queue: "hello",
                        autoAck: true,
                        consumer: consumer);

                    Thread.Sleep(1000);
                }
            }
        }   
    }
}