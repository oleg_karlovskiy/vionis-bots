﻿using ChatBotService.Config;
using NLog;
using VionisBotsKernel.Common;
using VionisBotsKernel.Telegram;
using VionisDevKernel;
using VionisDevKernel.Collections;
using VionisDevKernel.Utils;

namespace ChatBotService
{
    sealed class Program : BaseEntryPoint
    {
        public Logger log = LogManager.GetCurrentClassLogger();

        public readonly IChatBotConfig Settings;

        static void Main(string[] args)
        {
            var pr = new Program();
            pr.SetupLogger();
            pr.SetupConfigReader("settings.json", args.GetOrDefault(0).SafeBoolParse());
            pr.SetupAndStart();
        }

        private void SetupLogger()
        {
            NLogInstaller.Build();
            log.Info("NLogger successfully builded");
        }

        private void SetupConfigReader(string configName, bool initializateAllFields)
        {
            ConfigReaderInstaller.Setup(Settings, configName, initializateAllFields);
            log.Info("NetConfig successfully builded");
        }


        private BotCore botCore;
        protected override void StartMainLogic()
        {
            //setup core
            botCore = new BotCore();
            log.Info("CoreInit Started");

            //telegram setup
            TelegramHandler.Instance.Setup(Settings.TelegramBotToken, botCore);
            log.Info("Telegram Handler Started");
        }

        protected override void Shutdown()
        {
            log.Info("Initiate GracefullyShutdown");
            var details = botCore.ShutdownAll();
            if (details.IsOk)
                log.Info("GracefullyShutdown complete successfully");
            else
                log.Error($"GracefullyShutdown does`not complete correctly: {details.Info}");
        }
    }
}
