﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NLog;
using VionisDevKernel.Collections;
using VionisDevKernel.Utils;

namespace VionisMessagesBus
{
    public abstract class BaseRequestsHandler<TRequest> : BaseCommonHandler<TRequest>
    {
        public static Logger log = LogManager.GetCurrentClassLogger();

        private static int messageSnapshotsLimit = 140;
        private readonly SimpleMap<Task, TRequest> messageSnapshots = new SimpleMap<Task, TRequest>(messageSnapshotsLimit);

        private static int failedMessagesLimit = messageSnapshotsLimit;
        private readonly List<TRequest> failedMessages = new List<TRequest>(failedMessagesLimit);

        protected override Details HandleThreadSafe(TRequest message)
        {
            if (messageSnapshots.Size == messageSnapshotsLimit)
                return Details.Fail("LimitReached: messageSnapshots.Size == messageSnapshotsLimit");

            var task = SendMsg(message);
            if (task == null)
                return Details.Fail("Fail send message! task is null");
            messageSnapshots.Add(task, message);
            return Details.Ok();
        }

        protected abstract Task SendMsg(TRequest message);

        public struct SendedMessagesInfo
        {
            public int UnsentTasks;
            public int FailedTasks;
            public bool isOk;
        }

        public SendedMessagesInfo CheckSendedMessages()
        {
            SendedMessagesInfo info;
            info.UnsentTasks = 0;
            info.FailedTasks = 0;
            info.isOk = true;
            for (int idx = 0; idx < messageSnapshots.Size; idx++)
            {
                var taskSnapshot = GetTaskFromSnapshots(idx);
                if (!taskSnapshot.IsCompleted)
                {
                    info.UnsentTasks++;
                    continue;
                }
                    

                if (taskSnapshot.IsFaulted)
                {
                    log.Error($"Unhandled exception in task from BaseRequestsHandler: {taskSnapshot.Exception}");
                    failedMessages.Add(GetMessageFromSnapshots(idx));
                    info.FailedTasks++;
                }

                messageSnapshots.RemoveAt(ref idx);
            }

            info.isOk = info.FailedTasks == 0 && info.UnsentTasks == 0;
            return info;    
        }

        public int ResendFailedMessages()
        {
            int failedCount = failedMessages.Count;
            for (int idx = 0; idx < failedMessages.Count; idx++)
            {
                var details = Handle(failedMessages[idx]);
                if (details.IsOk)
                    failedMessages.FastRemoveAt(ref idx);
                else
                    log.Fatal($"Error in basic resend logic: {details.Info}");
            }
            return failedCount;    
        }

        private Task GetTaskFromSnapshots(int i) => messageSnapshots.Get1(i);
        private TRequest GetMessageFromSnapshots(int i) => messageSnapshots.Get2(i);

        protected override Details HandleAllThreadSafe()
        {
            CheckSendedMessages();
            ResendFailedMessages();
            return Details.Ok();
        }

        protected override Details HandleAllAfterShutdownThreadSafe()
        {
            var info = CheckSendedMessages();
            return info.isOk ? Details.Ok() : Details.Fail("Failed Messages: " + info.FailedTasks + "; UnsentMessages: " + info.UnsentTasks + ";"); //TODO: dump failed messages in this to file
        }
    }
}