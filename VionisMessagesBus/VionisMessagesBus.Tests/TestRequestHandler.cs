﻿using System;
using System.Threading.Tasks;
using NLog;

namespace VionisMessagesBus.VionisMessagesBus.Tests
{
    public class TestRequestHandler<T> : BaseRequestsHandler<T>
    {
        public static Logger log = LogManager.GetCurrentClassLogger();

        protected override Task SendMsg(T message)
        {
            var t = new Task(() =>
            {
                if (!message.ToString().Contains("["))
                    log.Info("socket sended success!: " + message);
                else
                {                    
                    throw new Exception("failed socket send: " + message);
                }
                    
            });
            t.Start();
            return t;
        }
    }
}