﻿using System.Threading;
using VionisDevKernel.VionisDevKernel.Tests;
using Xunit;
using Xunit.Abstractions;

namespace VionisMessagesBus.VionisMessagesBus.Tests
{
    public class RequestsHandlerTest : BaseTest
    {
        public RequestsHandlerTest(ITestOutputHelper outq) : base(outq)
        {
        }

        [Fact]
        public void FailedResend()
        {
            var requestHandler = new TestRequestHandler<string>();
            for (int i = 0; i < 4; i++)
            {
                requestHandler.Handle(i % 2 == 0 ? "qwe" + i : "qwe[" + i);
            }

            Thread.Sleep(3000);

            requestHandler.CheckSendedMessages();
            int failedCount = requestHandler.ResendFailedMessages();
            Assert.Equal(2, failedCount);   
        }

    }
}