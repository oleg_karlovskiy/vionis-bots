﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NLog;
using VionisDevKernel.Utils;
using VionisDevKernel.VionisDevKernel.Tests;
using Xunit;
using Xunit.Abstractions;

namespace VionisMessagesBus.VionisMessagesBus.Tests
{
    public class MultiThreadHandlerTest : BaseTest
    {
        public static Logger log = LogManager.GetCurrentClassLogger();

        public MultiThreadHandlerTest(ITestOutputHelper outq) : base(outq)
        {
        }

        [Fact]
        public void Main()
        {
            const int threadsCount = 2; // try change this
            var requestHandler = new TestRequestHandler<MockSimpleRequest>();
            var messagesHandler = new MultiThreadHandler<MockSimpleMessage, MockSimpleRequest>(threadsCount);

            messagesHandler.Start(typeof(MockMessageHandler), requestHandler);
            int threadsStarted = messagesHandler.GetCountAliveThreads();

            var messagesCount = 10; // try change this
            for (int i = 0; i < messagesCount; i++)
            {
                messagesHandler.Read(new MockSimpleMessage() {message = "msg" + i});    
            }
            
            Thread.Sleep(5000); // the more messagesCount, the more this delay

            var details = messagesHandler.ShutDownGracefully();

            XunitExtensions.EqualAsrt(true, details.IsOk, "details.IsOk: Info: " + details.Info);

            //sort and analyze log
            var lines = GetConsoleOutput().Split('\n');
            
            var handleprocessmessages = new List<string>();
            var sendedrequests = new List<string>();
            var handleCompletes = new List<string>();
            var socketSuccessHandlesRequests = new List<string>();
            var failedMessagesHandlesRequests = 0;
            foreach (var line in lines)
            {
                if (line.Contains("HandleProcess Message:"))
                    handleprocessmessages.Add(line);
                else if (line.Contains("Send request ("))
                    sendedrequests.Add(line);
                else if (line.Contains("HandleCOMPLETE Message:"))
                    handleCompletes.Add(line);
                else if (line.Contains("socket sended success!: "))
                    socketSuccessHandlesRequests.Add(line);
                else if (line.Contains("Failed Messages: "))
                    failedMessagesHandlesRequests = int.Parse(line.Split(new[] { "Failed Messages: ", ";" }, StringSplitOptions.None)[1]);
            }
            
            handleprocessmessages.Sort((q1, q2) => 
                                int.Parse(q1.Split(new []{ " (iter " }, StringSplitOptions.None)[1].Split(')')[0])
                    .CompareTo(int.Parse(q2.Split(new []{ " (iter " }, StringSplitOptions.None)[1].Split(')')[0])));
            
            sendedrequests.Sort((q1, q2) => 
                                int.Parse(q1.Split(new []{ "Send request (iter " }, StringSplitOptions.None)[1].Split(')')[0])
                    .CompareTo(int.Parse(q2.Split(new []{ "Send request (iter " }, StringSplitOptions.None)[1].Split(')')[0])));
            
            handleCompletes.Sort((q1, q2) => 
                                int.Parse(q1.Split(new []{ "HandleCOMPLETE Message: msg" }, StringSplitOptions.None)[1].Split(null)[0])
                    .CompareTo(int.Parse(q2.Split(new []{ "HandleCOMPLETE Message: msg" }, StringSplitOptions.None)[1].Split(null)[0])));
            
            socketSuccessHandlesRequests.Sort((q1, q2) => 
                                int.Parse(q1.Split(new []{ "frm msg: Message: msg" }, StringSplitOptions.None)[1].Split(',')[0])
                    .CompareTo(int.Parse(q2.Split(new []{ "frm msg: Message: msg" }, StringSplitOptions.None)[1].Split(',')[0])));


            var final = string.Join("\n", new[] {
                
                handleprocessmessages.ToArray(), 
                sendedrequests.ToArray(), 
                handleCompletes.ToArray(), 
                socketSuccessHandlesRequests.ToArray()

            }.SelectMany(x => x).Distinct());

            output.WriteLine("\nFormattedPrinted: \n"+final+"\n");
            
            XunitExtensions.EqualAsrt(threadsCount, threadsStarted, "threadsCount");
            XunitExtensions.EqualAsrt(messagesCount * REPEAT_MOCKHANDLER_COUNT - messagesCount * SEND_MOCKHANDLER_COUNT, handleprocessmessages.Count, 
                "messagesCount * REPEAT_MOCKHANDLER_COUNT - messagesCount * SEND_MOCKHANDLER_COUNT");
            XunitExtensions.EqualAsrt(messagesCount * SEND_MOCKHANDLER_COUNT, sendedrequests.Count, "messagesCount * SEND_MOCKHANDLER_COUNT");
            XunitExtensions.EqualAsrt(messagesCount, handleCompletes.Count, "messagesCount");
            XunitExtensions.EqualAsrt(messagesCount * 2, socketSuccessHandlesRequests.Count, "messagesCount * 2");
            XunitExtensions.EqualAsrt(messagesCount, failedMessagesHandlesRequests, "messagesCount");
        }

        private const int REPEAT_MOCKHANDLER_COUNT = 30;
        private const int SEND_MOCKHANDLER_COUNT = 3;

        public class MockMessageHandler : ICoreMessageHandler<MockSimpleMessage, BaseCommonHandler<MockSimpleRequest>>
        {
            public Details Handle(MockSimpleMessage message, BaseCommonHandler<MockSimpleRequest> commonHandler)
            {
                for (int i = 0; i < REPEAT_MOCKHANDLER_COUNT; i++)
                {
                    if (i > REPEAT_MOCKHANDLER_COUNT - SEND_MOCKHANDLER_COUNT - 1)
                    {
                        //send handles request
                        var sR = new MockSimpleRequest();
                        sR.request = "req frm msg: " + message + ", : " + i + (i % 2 == 0 ? "[" : "]");
                        var details = commonHandler.Handle(sR);
                        if (details.IsOk)
                            log.Info("Send request (iter " + i + ")" + " From " + message + "; " + Thread.CurrentThread.Name);
                        else
                            log.Info("Send request fail (iter " + i + ")" + " From " + message + "; .... Details: " + details.Info + Thread.CurrentThread.Name);
                    }
                    else
                        log.Info("HandleProcess " + message + " (iter " + i + ") From " + Thread.CurrentThread.Name);
                }

                log.Info("HandleCOMPLETE " + message + " From " + Thread.CurrentThread.Name);

                return Details.Ok();
            }
        }

        public class MockSimpleMessage
        {
            public string message;

            public override string ToString()
            {
                return "Message: " + message;
            }
        }

        public class MockSimpleRequest
        {
            public string request;

            public override string ToString()
            {
                return "Request: " + request;
            }
        }
    }
}