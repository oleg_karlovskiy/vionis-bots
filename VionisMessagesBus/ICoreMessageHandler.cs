﻿using VionisDevKernel.Utils;

namespace VionisMessagesBus
{
    public interface ICoreMessageHandler<TMessage, TCommonHandler>
        where TMessage : class
        where TCommonHandler : class
    {
        Details Handle(TMessage message, TCommonHandler commonHandler);
    }
}