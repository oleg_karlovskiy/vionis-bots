﻿using VionisDevKernel.Utils;

namespace VionisMessagesBus
{
    /// <summary>
    /// Thread safe wrapper
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseCommonHandler<T>
    {
        private readonly object sync = new object();

        private volatile bool isOkState = true;

        public bool IsOkState { get => isOkState; protected set => isOkState = value; }

        public Details Handle(T handlingObject)
        {
            lock (sync)
                return HandleThreadSafe(handlingObject);
        }

        protected abstract Details HandleThreadSafe(T handlingObject);

        public Details HandleAll()
        {
            lock (sync)
                return HandleAllThreadSafe();
        }

        protected abstract Details HandleAllThreadSafe();

        public Details HandleAllAfterShutdown()
        {
            lock (sync)
                return HandleAllAfterShutdownThreadSafe();
        }

        protected abstract Details HandleAllAfterShutdownThreadSafe();
    }
}