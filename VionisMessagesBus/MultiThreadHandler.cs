﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using NLog;
using VionisDevKernel.Utils;

namespace VionisMessagesBus
{
    /// <summary>
    /// Allows you to parallelize the work
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    /// /// <typeparam name="TCommonHandler">Common handler app logic</typeparam>
    /// <typeparam name="TCommonObject">Object handling TCommonHandler</typeparam>
    public sealed class MultiThreadHandler<TMessage, TCommonObject> : IDisposable
        where TMessage : class
        where TCommonObject : class
    {
        public static Logger log = LogManager.GetCurrentClassLogger();

        private readonly object messagesHandlerCreatingSync = new object();
        private readonly object getCountAliveSync = new object();
        private readonly object readSync = new object();

        /// <summary>
        /// Limit messagesToRead. If limit reached - then program blocking until Business Workers handling all
        /// </summary>
        private static int messagesLimit = 100;

        /// <summary>
        /// BlockingCollection - Provides blocking and bounding capabilities for thread-safe collections that implement IProducerConsumerCollection.
        /// - "Read" method adding messages to collection
        /// - In workers loops always calling "Take", then will handle messages
        /// </summary>
        private readonly BlockingCollection<TMessage> messagesToRead = new BlockingCollection<TMessage>(messagesLimit);

        /// <summary>
        /// Messages handler.
        /// </summary>
        private Type messagesHandlerType;

        /// <summary>
        /// Common handler app logic
        /// </summary>
        private BaseCommonHandler<TCommonObject> commonHandler;

        /// <summary>
        /// All started threads(BusinessWorkers) in this handler
        /// </summary>
        private Thread[] threads;

        /// <summary>
        /// Threads count. (Count cores / 2) recommended in dev environment. (Count cores) - in prod!
        /// </summary>
        private int threadsCount;

        /// <summary>
        /// Switches this to true in stopping workers
        /// </summary>
        private volatile bool workersShutdownTrigger;

        public MultiThreadHandler(int threadsCount = 4)
        {
            this.threadsCount = threadsCount;
        }
        
        public void Start(Type messagesHandlerType, BaseCommonHandler<TCommonObject> commonHandler)
        {
            if (threads != null)
                throw new Exception("Threads started");

            this.messagesHandlerType = messagesHandlerType ?? throw new Exception("MessageHandlerType == null");
            this.commonHandler = commonHandler ?? throw new Exception("CommonHandler == null");

            workersShutdownTrigger = false;
            threads = new Thread[threadsCount];
            for (int i = 0; i < threadsCount; i++)
            {
                bool isCommonHandlingTread = i == 0;
                threads[i] = new Thread(() => StartMainHandlerLoop(isCommonHandlingTread)) { Name = "BusinessWorker " + (i + 1) };
                threads[i].Start();             
            }
        }

        public Details Read(TMessage msg)
        {
            lock (readSync)
            {
                try
                {
                    messagesToRead.Add(msg);
                }
                catch (InvalidOperationException e)
                {
                    return Details.Fail(e.ToString());
                }
                return Details.Ok();
            }
        }

        /// <summary>
        /// Starts handling messages loop
        /// </summary>
        public void StartMainHandlerLoop(bool isCommonHandler)
        {
            ICoreMessageHandler<TMessage, BaseCommonHandler<TCommonObject>> messagesHandler;
            lock (messagesHandlerCreatingSync)
            {
                try
                {
                    messagesHandler = (ICoreMessageHandler<TMessage, BaseCommonHandler<TCommonObject>>)Activator.CreateInstance(messagesHandlerType);
                }
                catch (InvalidCastException e)
                {
                    log.Fatal("BusinessWorker Start failed: bad messagesHandler... SHUTDOWN!!!: \n" + e);
                    return;
                }
            }       

            log.Info("Has started " + Thread.CurrentThread.Name);

            //main loop
            while (!workersShutdownTrigger || messagesToRead.Count != 0 || !commonHandler.IsOkState)
            {
                if (isCommonHandler)
                {
                    try
                    {
                        var details = commonHandler.HandleAll();
                        if (!details.IsOk)
                            log.Error("from " + Thread.CurrentThread.Name + ":\n" + details.Info + "\n");
                    }
                    catch (Exception e)
                    {
                        log.Fatal("CommonHandler Error from " + Thread.CurrentThread.Name + ":\n" + e + "\n");
                        return;
                    }
                }

                const int takeTimeoutMs = 1000;
                messagesToRead.TryTake(out TMessage msg, takeTimeoutMs);
                if (msg == null)    //if take timeout reached - go to next iter
                    continue;

                try
                {
                    var details = messagesHandler.Handle(msg, commonHandler);
                    if (!details.IsOk)
                        log.Error("from " + Thread.CurrentThread.Name + ":\n" + details.Info + "\n");
                }
                catch (Exception e)
                {
                    log.Fatal("from " + Thread.CurrentThread.Name + ":\n" + e + "\n");
                }
            }

            if (isCommonHandler)
            {
                //wait to stop all other threads
                while (threads.Count(q => q.IsAlive) > 1)
                {
                    Thread.Sleep(300);
                }

                try
                {
                    var details = commonHandler.HandleAllAfterShutdown();
                    if (!details.IsOk)
                        log.Error("HandleAllAfterShutdown CommonHandler from " + Thread.CurrentThread.Name + ":\n" + details.Info + "\n");
                }
                catch (Exception e)
                {
                    log.Fatal("HandleAllAfterShutdown CommonHandler Error from " + Thread.CurrentThread.Name + ":\n" + e + "\n");
                    return;
                }
            }

            log.Info("Has stopped " + Thread.CurrentThread.Name);
        }

        /// <summary>
        /// Waits for all workers end data processing, then returns control
        /// </summary>
        /// <param name="shutdownSecLimit">Data processing time limit</param>
        /// <returns>If threads don't closed at limit time - return false, else true</returns>
        public Details ShutDownGracefully(int shutdownSecLimit = 10)
        {
            var shutdownSecLeft = 0;
            workersShutdownTrigger = true;
            while (GetCountAliveThreads() != 0 && shutdownSecLeft != shutdownSecLimit)
            {
                Thread.Sleep(1000);
                shutdownSecLeft++;
            }
            return shutdownSecLeft != shutdownSecLimit ? Details.Ok() : Details.Fail("Fail shutdown... " + threads.Count(q => q.IsAlive) + " Thread(s) is running currently");
        }

        public int GetCountAliveThreads()
        {
            lock (getCountAliveSync)
                return threads.Count(q => q.IsAlive);
        }

        public void Dispose()
        {
            messagesToRead?.Dispose();
        }
    }
}